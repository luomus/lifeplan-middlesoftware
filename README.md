File transfer software for use in Lifeplan project for transfering image- and audio-files from Nextcloud shared folder, retrieved from lifeplan backend, to Allas buckets.

### Running on local machine

1. Genrate api-token to lifeplan-backend, if wanted generate api-token to lifeplan-monitor.
2. Copy and rename .env.example to .env and fill all the vaiables therein (except lifeplan-monitor variables which are optional).
3. Run the software with `docker compose up`, which will result in a single file transfer run.

### Opemshift deployment, using Luomus dockerhub
1. Build image for with `docker build . -t luomus/lifeplan-middlesoftware:latest`
2. Log in to docker
3. Push the image to dockerhub with `docker push luomus/lifeplan-middlesoftware:latest`
4. Add secrets and configmaps both named middlesoftware, see cron.yaml for split.
5. Import cron.yaml to begin running middlesoftware as cronjob. 
6. If adding multiple cronjobs, staggering the schedules is advisable to avoid accidental duplicate work.
from requests_oauthlib import OAuth2Session
import json
import os

class   LifeplanService:
  def __init__(self):
    self.url = os.environ['LIFEPLAN_API_URL']
    self.headers = {
      'Content-Type': 'application/json', 
      'Accept': 'application/json',
    }
    token = {
      'token_type': 'Bearer',
      'access_token': os.environ['LIFEPLAN_API_TOKEN']
    }

    self.session = OAuth2Session(token = token)
  
  def get_status(self, id):
    res = self.session.get(
      url = self.url + '/{}/status'.format(id),
      headers = self.headers
    )

    res.raise_for_status()

    return res.text

  #get new items from lifeplan backend with processing_status as selected
  def get_items(self, **kwargs):
    params = {}

    start_from = kwargs.get('start_from', None)
    if start_from:
      params['from'] = start_from

    processing_status = kwargs.get('processing_status', None)
    if processing_status:
      params['processing_status'] = processing_status

    page = kwargs.get('page', 1)
    params['page'] = page

    res = self.session.get(
      url = self.url, 
      headers = self.headers,
      params = params
    )

    res.raise_for_status()

    #convert result to dict, extract result from data-field
    res_object = json.loads(res.text)

    result = res_object['data']

    #if current page is not last, call get_items recursively, add result from that to current result until last page reached
    if(res_object['meta']['last_page'] > page):
      next_page_result = self.get_items(start_from = start_from, processing_status = processing_status, page = page + 1)
      result.extend(next_page_result)

    return result

  #update the activity with id, kwargs expected to contain either updated resource_path or processing status
  def update_status(self, id, **kwargs):
    url = self.url + '/{}'.format(id)

    payload = {}

    #checks if processing_status is present in kwargs, and adds it to payload if it is  
    processing_status = kwargs.get('processing_status', None)
    if processing_status:
      payload['processing_status'] = processing_status

    #checks if resource_path is present in kwargs, and adds it to payload if it is  
    resource_path = kwargs.get('resource_path', None)
    if resource_path:
      payload['resource_path'] = resource_path
    
    res = self.session.patch(
      url,
      headers = self.headers,
      json = payload
    )
    res.raise_for_status()
  
    return json.loads(res.text)['data']
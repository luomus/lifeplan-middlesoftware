import os
import boto3
import botocore

class AllasService:
  def __init__(self):
    self.s3 = boto3.resource('s3', endpoint_url=os.environ['S3_ENDPOINT'])
    self.client = self.s3.meta.client

  def get_free_bucket_name(self, bucket_name, iteration = 1):
    new_bucket_name = bucket_name

    if (iteration >1):
      new_bucket_name = '{}-{}'.format(bucket_name, iteration)

    try:
      self.client.head_bucket(
        Bucket=new_bucket_name
      )
    except botocore.client.ClientError as err:
      if err.response['Error']['Code'] == '404':
        return new_bucket_name
      else:
        raise err


    count = sum(1 for _ in self.s3.Bucket(new_bucket_name).objects.all())

    if (count < 490000):
      return new_bucket_name


    return self.get_free_bucket_name(bucket_name, iteration + 1)

  def create_bucket(self, bucket_name):
    self.client.create_bucket(
      Bucket=bucket_name,
      GrantFullControl='id={}'.format(os.environ['S3_ACL_FULL']),
      GrantRead='id={}'.format(os.environ['S3_ACL_READ'])
    )

  def upload(self, local_path, container, allas_path):
    self.client.upload_file(local_path, container, allas_path, ExtraArgs={
      'GrantFullControl': 'id={}'.format(os.environ['S3_ACL_FULL']),
      'GrantRead': 'id={}'.format(os.environ['S3_ACL_READ'])
    })
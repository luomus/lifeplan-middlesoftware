from requests_oauthlib import OAuth2Session
import os

class   MonitorService:
  def __init__(self):
    self.url = os.environ['MONITOR_API_URL']
    self.headers = {
      'Content-Type': 'application/json', 
      'Accept': 'application/json',
    }
    token = {
      'token_type': 'Bearer',
      'access_token': os.environ['MONITOR_API_TOKEN']
    }

    self.session = OAuth2Session(token = token)
  
  def create_instance(self, instance):
    url = self.url + '/instance'

    self.session.post(
      url,
      headers = self.headers,
      json = {
        'instance': instance
      }
    )

  def update_instance(self, instance, include_activities=False, no_activities=False):
    url = self.url + '/instance/{}'.format(instance['id'])

    self.session.patch(
      url,
      headers = self.headers,
      json = {
        'include_activities': include_activities,
        'no_activities': no_activities,
        'instance': instance
      }
    )

  def update_activity(self, activity):
    url = self.url + '/activity/{}'.format(activity['id'])

    self.session.patch(
      url,
      headers = self.headers,
      json = {
        'activity': activity
      }
    )
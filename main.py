from controllers.middlesoftware import MiddlesoftwareController
import os
import logging
import sys

def main():
  logging.basicConfig(stream=sys.stdout, level=os.environ.get('LOGGING_LEVEL', 'DEBUG'), format='%(asctime)s :: %(levelname)s :: %(message)s :: in %(filename)s line %(lineno)s')
  logger = logging.getLogger(__name__)

  ms = MiddlesoftwareController()
  ms.transfer_files()

if __name__ == '__main__':
  main()

import os
import uuid
import logging

class MonitorController:
  def __init__(self, MonitorService):
    self.MonitorService = MonitorService
    self.init_failure = False
    self.instance = {}
    self.errors = False
    self.logger = logging.getLogger(__name__)

    if(os.environ['MONITOR_API_URL'] == ''):
      self.logger.error('No monitor endpoint env variable present, skipping updating monitor.')
      self.init_failure = True

  def begin_instance(self):
    if(self.init_failure):
      return

    self.instance = {
      'id': str(uuid.uuid4()),
      'status': 'instance.status.0',
      'activities': []
    }

    try:
      self.MonitorService.create_instance(self.instance)
    except Exception as err:
      self.logger.error('Failure to send instance start to monitor API, ignoring monitoring, error: {}'.format(err))
      self.init_failure = True

  def add_activities(self, activities):
    if(self.init_failure):
      return
    
    newActivities = []
    for activity in activities:
      newActivities.append({
        'id': activity['id'],
        'uuid': activity['uuid'],
        'status': 'activity.status.0',
        'currentSize': None,
        'totalSize': None,
        'duration': None,
        'notes': None,
        'processedBy': None
      })

    self.instance['activities'] = newActivities

    try:
      self.MonitorService.update_instance(self.instance, True)
    except Exception as err:
      self.logger.error('Failure to send list of activities to monitor API, error: {}'.format(err))

  #sets status of activity internally as passed, does not communicate with monitoring API
  def pass_activity(self, idx):
    if(self.init_failure):
      return

    activity = self.instance['activities'][idx]
    activity['status'] = 'passed'

  #sets status of activity and adds notes if given i.e. in errors, and updates to monitoring API
  def update_activity(self, idx, status, notes = None, current_size = None, total_size = None, duration=None):
    if(self.init_failure):
      return

    activity = self.instance['activities'][idx]
    activity['status'] = status

    if(status == 'activity.status.1'):
      activity['processedBy'] = self.instance['id']
    elif(status == 'activity.status.4'):
      self.errors = True

    if(notes):
      activity['notes'] = notes
    
    if(current_size):
      activity['currentSize'] = current_size
  
    if(total_size):
      activity['totalSize'] = total_size

    if(duration):
      activity['duration'] = duration

    try:
      self.MonitorService.update_activity(activity)
    except Exception as err:
      self.logger.error('Failure to send activity update to monitor API, error: {}'.format(err))

  def finish_instance(self, failed = False, notes = None, no_activities = False):
    if(self.init_failure):
      return
    
    if(notes):
      self.instance['notes'] = notes

    #if instance itself failed, send status update
    if(failed):
      self.instance['status'] = 'instance.status.3'

      try:
        self.MonitorService.update_instance(self.instance, False, no_activities)
      except Exception as err:
        self.logger.error('Failure to send instance end to monitor API, error: {}'.format(err))
      return

    #if error-flag has been set by one of the activities failing, determine if failure was total or partial
    if(self.errors):
      activities = self.instance['activities']
      counter = 0

      #count failed and passed activities for determining if instance was total failure
      for activity in activities:
        if(activity['status'] == 'activity.status.4'):
          counter += 1 
        elif(activity['status'] == 'passed'):
          counter += 1

      if(counter == len(activities)):
        self.instance['status'] = 'instance.status.3'
        self.instance['notes'] = 'Failure of all activities'
      else:
        self.instance['status'] = 'instance.status.2'
        self.instance['notes'] = 'Failure of some activities'
    else:
      self.instance['status'] = 'instance.status.1'

    #try to send update to monitoring API
    try:
      self.MonitorService.update_instance(self.instance, False, no_activities)
    except Exception as err:
      self.logger.error('Failure to send instance end to monitor API, error: {}'.format(err))
from services.allas import AllasService
from services.lifeplan import LifeplanService
from services.monitor import MonitorService
from controllers.monitor import MonitorController
import owncloud
import os
import json
import logging
import tempfile
import re
import datetime
import stopit

class MiddlesoftwareController:
  def __init__(self):
    self.owncloud = owncloud
    self.lifeplan = LifeplanService()
    self.allas = AllasService()
    self.monitorService = MonitorService()
    self.monitor = None
    self.logger = logging.getLogger(__name__)
    stopit_logger = logging.getLogger('stopit')
    stopit_logger.setLevel(logging.ERROR)
    self.transfer_timeout = os.environ['TRANSFER_TIMEOUT']

  def transfer_files(self):
    self.monitor = MonitorController(self.monitorService)

    #send start signal to monitoring backend
    self.monitor.begin_instance()
  
    #get new set of items
    try:
      self.logger.info('Querying new activities from Lifeplan')
      new_items = self.lifeplan.get_items(processing_status = 'unprocessed')

      #return if no new activities available
      if(not new_items or len(new_items) <= 0):
        self.monitor.finish_instance(False, 'No new activities', True)
        self.logger.info('No new items on Lifeplan')
        return

      #send list of activities to monitoring backend
      self.monitor.add_activities(new_items)
      self.logger.info('Loaded {} activities from Lifeplan'.format(len(new_items)))
    
    except Exception as err:
      self.monitor.finish_instance(True, 'Failure fetching activities.')
      self.logger.error('Failed on fetching activities: error: {}'.format(err))
      return

    for idx, item in enumerate(new_items):
      try:
        start_time = datetime.datetime.now()

        folder_name = item['uuid']
        resource_path = item['resource_path']

        self.logger.info('Activity {}, started processing.'.format(folder_name))

        #get items processing status from server
        try:
          processing_status = self.lifeplan.get_status(item['id'])
        #if status cannot be fetched pass activity marking it passed
        except Exception as err:
          self.monitor.pass_activity(idx)
          self.logger.info('Activity {} not processed, could not confirm its processing status.'.format(folder_name))
          continue

        #if current item is still marked unprocessed handle it in this instance
        if (processing_status != 'unprocessed'):
          self.monitor.pass_activity(idx)
          self.logger.info('Activity {} not processed, already in-progress somewhere else'.format(folder_name))
          continue

        #environmental variable to determine of bucket name will have test-prefix
        if (os.environ['LIFEPLAN_TESTING'] == 'true'):
          bucket_name = 'test-'
        else:
          bucket_name = ''

        #construct bucket name using equipment code, site id and recoding date
        try:
          if (item['equipment_code'][0].lower() == 'n'):
            bucket_name += 'A'
          elif (item['equipment_code'][0].lower() == 'j'):
            bucket_name += 'I'
          else:
            self.monitor.update_activity(idx, 'activity.status.4', 'Unexpected first letter of equipment code.')
            self.lifeplan.update_status(id = item['id'], processing_status = 'failed')
            self.logger.error('Activity {} not processed, unexpected first letter of equipment code.'.format(folder_name))
            continue
        
          bucket_name += '-{}-{}'.format(item['site_id'], item['recorded_at'][2:4])

          #check that bucket has room for more files, if not increment bucket name
          bucket_name = self.allas.get_free_bucket_name(bucket_name)
          self.allas.create_bucket(bucket_name)
          
        except Exception as err:
          self.monitor.update_activity(idx, 'activity.status.4', 'Error creating bucket.')
          self.lifeplan.update_status(id = item['id'], processing_status = 'failed')
          self.logger.error('Activity {} not processed, error creating bucket, error: {}'.format(folder_name, err))
          continue
        
        #Change activity's status on lifeplan and monitoring app
        self.lifeplan.update_status(id = item['id'], processing_status = 'in-progress')
        self.monitor.update_activity(idx, 'activity.status.1')

        #try to get shared item from Nextcloud
        try:
          oc = self.owncloud.Client.from_public_link(resource_path)
          children_list = oc.list('')
        except Exception as err:
          self.monitor.update_activity(idx, 'activity.status.4', 'Failure opening share link on Nextcloud.')
          self.lifeplan.update_status(id = item['id'], processing_status = 'failed')
          self.logger.error('Activity {} could not be processed, error opening Nextcloud share link, error: {}'.format(folder_name, err))
          continue

        #check that shared folder is not empty
        if (len(children_list) <= 0):
          self.monitor.update_activity(idx, 'activity.status.4', 'No files in share target.')
          self.lifeplan.update_status(id = item['id'], processing_status = 'failed')
          self.logger.error('Activity {} could not be processed, no files in shared folder'.format(folder_name))
          continue

        #get total size of activity files for progress reporting, initialize transferred size counter, 
        #check that no directories are present, or unexpected file extensions
        activity_size = 0
        current_size = 0
        try:
          for child in children_list:
            if(child.is_dir()):
              self.monitor.update_activity(idx, 'activity.status.4', 'Directory detected in share target.')
              self.logger.error('Activity {} could not be processed, Folder in shared folder.'.format(folder_name))
              raise StopIteration
            if(not re.search('^.+\.(?i)(dat|tdat|jpg|jpeg|txt)$', child.get_name())):
              self.monitor.update_activity(idx, 'activity.status.4', 'Unaccepted file extension in share target.')
              self.logger.error('Activity {} could not be processed, unaccepted file extension in share target'.format(folder_name))
              raise StopIteration

            activity_size += child.get_size()
        except StopIteration:
          self.lifeplan.update_status(id = item['id'], processing_status = 'failed')
          continue

        #loop over children for transfer, store downloaded files in temporary folder
        with tempfile.TemporaryDirectory() as tmp_dir:
          #send initial size data to monitor
          self.monitor.update_activity(idx, 'activity.status.1', current_size=0, total_size=activity_size)

          for f_idx, child in enumerate(children_list):
            #on every 10 transferred files update monitor on currentöy transfered data
            current_size += child.get_size()
            if(f_idx % 10 == 0 and activity_size != 0):
              self.monitor.update_activity(idx, 'activity.status.1', current_size=current_size)
            
            child_name = child.get_name()
            nextc_path = child.get_path() + child_name
            allas_path = os.path.join(folder_name, child_name)
            local_path = os.path.join(tmp_dir, child_name)

            #wrap download-upload into stopit SignalTimeout to enable restart the process up to the limit of 5 if it does not complete on acceptable time
            tries = 0
            exit_condition = 0

            while(tries < 5):
              with stopit.SignalTimeout(self.transfer_timeout) as cm:
                #try to download file
                try:
                  self.logger.info('Activity {}, file {}, starting download.'.format(folder_name, child_name))
                  oc.get_file(nextc_path, local_path)
                  self.logger.info('Activity {}, file {}, finished download'.format(folder_name, child_name))
                except stopit.TimeoutException as err:
                  raise err
                except Exception as err:
                  self.monitor.update_activity(idx, 'activity.status.4', 'Failed on file download.')
                  self.lifeplan.update_status(id = item['id'], processing_status = 'failed')
                  self.logger.error('Activity {} could not be processed, error downloading file {}, error: {}'.format(folder_name, child_name, err))
                  cm.cancel()
                  exit_condition = 1
                  break

                #Try to upload file
                try:
                  self.logger.info('Activity {}, file {}, starting upload to bucket {}'.format(folder_name, child_name, bucket_name))
                  self.allas.upload(local_path, bucket_name, allas_path)
                  self.logger.info('Activity {}, file {}, finished upload to bucket {}'.format(folder_name, child_name, bucket_name))
                except stopit.TimeoutException as err:
                  raise err
                except Exception as err:
                  self.monitor.update_activity(idx, 'activity.status.4', 'Failed on file upload.')
                  self.lifeplan.update_status(id = item['id'], processing_status = 'failed')
                  self.logger.error('Activity {} could not be processed, error uploading file {}, error: {}'.format(folder_name, child_name, err))
                  cm.cancel()
                  exit_condition = 1
                  break

              #download-upload finished on time and didn't break out due to errors results in while loop being broken and exoit condition remaining 0
              if(cm):
                break

              #else increment tries and send update to monitor that a files transfer restarted
              self.monitor.update_activity(idx, 'activity.status.1', 'Restarted transfer of a file.')
              self.logger.info('Activity {}, file {}, timeout, restarting transfer.'.format(folder_name, child_name))
              tries += 1

            #if while loop exits normally, i.e. tries exceeds 5, mark activity as failed, as successive failures might point to an actuall error
            else:
              self.monitor.update_activity(idx, 'activity.status.4', 'File transfer failed, number of retries for transfer timeout exceeded.')
              self.lifeplan.update_status(id = item['id'], processing_status = 'failed')
              self.logger.error('Activity {} could not be processed, exceeded number of retries for transfer timeouts, file {}.'.format(folder_name, child_name))
              exit_condition = 1
            
            #handle exit condition, anything else than 
            if (exit_condition != 0):
              break

            #delete local file
            os.remove(local_path)

          #if files were transfered successfully, create and upload metadata, delete files on nextcloud
          else:
            #Add metadata to a file as json, with resource path replaced with allas url-path and attempt to upload it.
            item['resource_path'] = 'a3s.fi/{}/{}'.format(bucket_name, folder_name)

            allas_path = os.path.join(folder_name, 'metadata.json')
            local_path = os.path.join(tmp_dir, 'metadata.json')

            with open(local_path, 'w') as metadata:
              json.dump(item, metadata, indent=2)

            #Try to upload metadata
            try:
              self.logger.info('Activity {}, file {}, starting upload to bucket {}'.format(folder_name, 'metadata.json', bucket_name))
              self.allas.upload(local_path, bucket_name, allas_path)
              self.logger.info('Activity {}, file {}, finished upload to bucket {}'.format(folder_name, 'metadata.json', bucket_name))
            except Exception as err:
              self.monitor.update_activity(idx, 'activity.status.4', 'Failed on metadata file upload.')
              self.lifeplan.update_status(id = item['id'], processing_status = 'failed')
              self.logger.error('Activity {} could not be processed, error uploading files, error: {}'.format(folder_name, err))
              continue

            #Inform monitoring backend of transitioning to deletion, uppdate lifeplan with success and resource path
            self.monitor.update_activity(idx, 'activity.status.2', current_size=activity_size)
            self.lifeplan.update_status(id = item['id'], processing_status = 'completed', resource_path=item['resource_path'])

            #delete files from nextcloud, last member of childre_list ignored, as it is metadata created above
            for child in children_list:
              child_name = child.get_name()
              nextc_path = child.get_path() + child_name
              
              try:
                oc.delete(nextc_path)
                self.logger.info('Activity {}, deleted file {}'.format(folder_name, child_name))
              except Exception as err:
                activity_duration = datetime.datetime.now() - start_time
                self.monitor.update_activity(idx, 'activity.status.3', 'Error deleting files.', duration=activity_duration.total_seconds() * 1000)
                self.logger.warning('Activity {}, ccould not delete file {}, error: {}'.format(folder_name, child_name, err))
                break
      
            #Inform monitoring backend that transfer is completed, set total elapsed time
            activity_duration = datetime.datetime.now() - start_time
            self.monitor.update_activity(idx, 'activity.status.3', duration=activity_duration.total_seconds() * 1000)
            self.logger.info('Activity {}, finished processing.'.format(folder_name))

      #if errror happened try to update status of activity in lifeplan and log error, continue to next activity 
      except Exception as err:
        self.monitor.update_activity(idx, 'activity.status.4', 'Unexpected error, check logs.')
        self.lifeplan.update_status(id = item['id'], processing_status = 'failed')
        self.logger.error('Activity {}, unexpected error: {}'.format(item['uuid'], err))

    self.monitor.finish_instance()
 